工具类

##介绍
    简单易用,小白写法. 缺点:只针对部分接口进行测试.

 
##使用方法
    alisdk demo:
    $aop = new AopClient ();
    $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
    $aop->appId = 'your app_id';
    $aop->rsaPrivateKey = '请填写开发者私钥去头去尾去回车，一行字符串';
    $aop->alipayrsaPublicKey='请填写支付宝公钥，一行字符串';
    $aop->apiVersion = '1.0';
    $aop->signType = 'RSA2';
    $aop->postCharset='GBK';
    $aop->format='json';
    $object = new stdClass();
    $object->out_trade_no = '20210823010101001b';
    $object->total_amount = 0.01;
    $object->subject = '测试商品';
    $object->buyer_id ='2088102146225135';
    $object->timeout_express = '10m';
    ////商品信息明细，按需传入
    // $goodsDetail = [
    //     [
    //         'goods_id'=>'goodsNo1',
    //         'goods_name'=>'子商品1',
    //         'quantity'=>1,
    //         'price'=>0.01,
    //     ],
    // ];
    // $object->goodsDetail = $goodsDetail;
    // //扩展信息，按需传入
    // $extendParams = [
    //     'sys_service_provider_id'=>'2088511833207846',
    // ];
    //  $object->extend_params = $extendParams;
    // //结算信息，按需传入
    // $settleInfo = [
    //     'settle_detail_infos'=>[
    //         [
    //             'trans_in_type'=>'defaultSettle',
    //             'amount'=>0.01,
    //         ]
    //     ]
    // ];
    // $object->settle_info = $settleInfo;
    // //二级商户信息，按需传入
    // $subMerchant = [
    //     'merchant_id'=>'2088600522519475',
    // ];
    // $object->sub_merchant = $subMerchant;
    // // 业务参数信息，按需传入
    // $businessParams = [
    //     'busi_params_key'=>'busiParamsValue',
    // ];
    // $object->business_params= $businessParams;
    // // 营销信息，按需传入
    // $promoParams = [
    //     'promo_params_key'=>'promoParamsValue'
    // ];
    // $object->promoParams = $promoParams;
    $json = json_encode($object);
    $request = new AlipayTradeCreateRequest();
    $request->setNotifyUrl('');
    $request->setBizContent($json);
    $result = $aop->execute ( $request);

    $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
    $resultCode = $result->$responseNode->code;
    if(!empty($resultCode)&&$resultCode == 10000){
    echo "成功";
    } else {
    echo "失败";
    }

    引入SDK后
    代码中
    $config = [
        'gatewayUrl' => 'https://openapi.alipay.com/gateway.do',
        'appId' => '2021002178XXXXX',
        'rsaPrivateKey'  =>'MIIEowICAAKCAQEAjo0VE3cc2M3opg9cEj+YRxRl0Zz8UFlETEP3RqcA0d8PFmGA59KpYvymHktUn+aGgPrs7BWZvbJkg2Z9Fy8qFcUetA4ooQOXYFGrlFkmW+R0kCqjRuhLMV7qxVxwWXmIK73KBuoTqaxP+/5wDN7rbx3dnlPVeejOZPRGAAK8i1klqAflDJhTnVPuTk9jyygOW7CGMM7uML9Hm7xMeMlSuekaMdwfYUUeoX1tfdtXvZBwubMRJnnii+j85wZVRXcPXdJkvx7v+bEOA1sQYjlcSAAOeG17JljRGv32ngnJLNrUpPa+rXKkpoc6Dlh6KKOCHnXPImRLahcPKpNe0r19HwIDAQABAoIBADuBzZCIorQmkL3xn3/GqGzTqQVtwS5RdV9Y5/rIcGEr+oJfRY6AtFEsIrR2webWNsPU+945zioQNEFeIU3yVGEuHnkPiApfE2wT68pLxOWLYtVj1zEce3cqplqpEgVn5F5OJe3o/ThRRDpvpprY2/NQxlj3qh/v/WjLSAVtHwQUHJVCyCtVtX5i+4X4cMAdj/IxVytq5Fmomw3HSCRFxUjcYdRM5KyVcIIvMc2rTp5YGHkxjnZS0HC4ngbMq2a1/W5tBpl2cbS7lBXjB7m3RKS7Wet2RcZffYLtUTDOK39PEmETsF15vMxY8V0LxKzuZr/FqESsvv4DzlG/YH0BHFECgYEA0mplfXfPX/XnTloXqiNRjQUVxWietNDExaJblYW+y+0PrYL7CVtM6va1fSYGg9Pd+RkdHRkw/m6pHC4AwZ45ZYDFynjKVd8Y3o0BtGs36z1FT9Wa0DEDH7N+S6Po90oRYxefx9Nzefs1ZXBnlGv9GyfoOLlcDAfAcCL25wKjkD0CgYEArW7vZxfwhp7eAAIauq4CyK51YrIMVM5BiuD4dIQU76dF6GtLE+BCE+STPO9MeAuLqfB6ogal0lu1rjxRhQYrCnvz1VWJLf7/LyFkXf9vzKtvBLclOdvQicMswpgRi7XPUqvYHtWPdJK/4Fnrt0yE+wOv5DRli0jJAMgKMNe9nIsCgYAXAedDBty4A21dVeCMi/wYzVuKEFx6LPG3In+dI9iu96blNBM6/K62am+B1ayUsY/t8t8KFX0T9QmsuSMACMsB3EjEUNze8twHSafMEIU2xaZUimi5JwLObpP6tVt93q10BltEQEy6I8u84cORqRUt6Pktaqcz8Y0LNeeFbvZviQKBgQCH0BynlcakFRpx1TQLRiMl9gA0loszigUndT77N0eR2GLVO9giK3ZzT4bBq/8s7pWjMY9G/H+0OF8Qn2mTCx5/akuqnv36x/obdUG8nkB1fTZF8gmzwmAO0YlmpWDwECLXcXyfwMtDFXwuexwsgT6Ntvhxc1qldQz6kKvyTa/NOwKBgCpTSk3eXwI4d7eog0PC8h0g0QbqvsgDMl2wt0TXVl0D1MzcQyFizPAAjzOuN6nMVJmQe9XJ1DWQLaqzvWrLRi7btHXpCfdW77raj3AsXae06kQ0/xNVXeub95RwDyinWZtVE4hl27aTU8kdOJlTjlZbi0IebFPd7fooq+og1Mr+',
        'alipayrsaPublicKey' => 'MIIBBjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkEFkX4Ls76HZb4l5N7Qc2r64ILkCI+6dSuLoXIn2nrXO58gc70IZXDcuGrAQRBOWeTmpvDRuwgYlFwS06A8OemHOXIokNRnYdrpsf4/5V/hJXQ3oTWfotOQ1tQ2rZwqG0aDdziDJJuEJoQH1ULjeWQFmfXxWHDk/KcjQscB7/C1Ko+uni7K2QSWZa5XNo2Pxt5xQL/Mfzx1TZ2Hwe90ueLNARjMWO4oCIiu03wDSxDVIMB36urMhPpzhXmq2EPjfqlR4yQ4qScSOIzvOgmwuTPnS5xyK6tJlzQ/yoJXqdDn6sCZOuOVsUmCWM4Zt84v1dxbNvZHt8nsDD/dii0tFxwIDAQAB',
        'apiVersion' => '1.0',
        'signType' => 'RSA2',
        'format' => 'JSON',
    ]
    或者在框架中直接配置
    $config = config('alipay');


    //统一收单交易创建
    $bizContent = [
        'out_trade_no' => '20210823010101002221',
        'total_amount' => '0.01',
        'subject' => '测试商品',
        'buyer_id' => '2088712279231011',
    ];

    $res = AlipayClient::create()
        ->setAop($config)
        ->setBizContent($bizContent)
        ->setMethod('alipay.trade.create')
        ->execute();    

    


